# App

## Deployed URL
  * https://p1-87lu.onrender.com
  
## Local Server Launch
  * Install dependencies with `mix deps.get`
  * Install and build tailwind with `mix tailwind.install ; mix tailwind default`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`
  * Visit [`localhost:4000`](http://localhost:4000) from browser.

## Render.com Configuration
### Environment variables
  * ELIXIR_VERSION = 1.14.2
  * ERLANG_VERSION = 25.1.1
  * SECRET_KEY_BASE = <mix phx.gen.secret output>

### Build & Deploy
  * Build Command : "./build.sh"
  * Start Command : "_build/prod/rel/app/bin/app start"
  
## Useful Links
  * https://dashboard.render.com/
  * https://poc-001-service.onrender.com/
  * https://github.com/phoenixframework/tailwind
  * https://hexdocs.pm/tailwind/Mix.Tasks.Tailwind.Install.html#content
  * https://andrewbarr.io/posts/removing-npm/show
  * https://pragmaticstudio.com/tutorials/adding-tailwind-css-to-phoenix
  * https://elixirforum.com/t/how-to-get-daisyui-and-phoenix-to-work/46612
  * https://elixirforum.com/t/tailwind-not-working-in-production-no-styles-just-plain-html/45192
  * https://tailwindcss.com/docs/installation

## Learning Resources
  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
